// S24 Activity Template:
	/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:


		let x = 2;
		let y = 3;
		const getCube = x ** y
		console.log(`the cube of ${x} is ${getCube}.`);



	/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


	// Code here:

		const adress = ["Polaris Subd, Road 3, Mt. View, Mariveles, Bataan"]

		const [adressA] = adress

		console.log(`I live at ${adressA}`)

	/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:

		const Animal = {
		AnimalName: "Cat",
		Claws: "Long Claws",
		EarType: "Long",
		age: 26
	}

	const {AnimalName, Claws, EarType, age} = Animal
	console.log(`This is a ${AnimalName}. It has ${Claws} and has a very ${EarType} ears. It's age is ${age}`)
console.log("")

	/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

	// Code here:



		const numbers = [1, 2, 3, 4, 5, 15];

		numbers.forEach((number) => 
		console.log(number));
/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

	// Code here:


		class Dog {
			constructor(name, age, breed) {
				this.name = "den"
				this.age = 14
				this.breed = "German Sheperd"
			}
		}

		const dog = new Dog()
		console.log(dog)

	